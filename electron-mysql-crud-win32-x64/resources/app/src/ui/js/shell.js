const { remote } = require("electron");
const main = remote.require("./main");
const exec = require("child_process").exec;

const shell = document.querySelector("#shell");

function ejecutaShell() {
  var a = 10;
  exec("src/shellScript/script.sh " + a, (err, stout) => {
    if (err) {
      throw err;
    }
    //console.log("comando ejecutado");
    console.log(stout);
    //renderSell(stout);
    let timerInterval;
    Swal.fire({
      title: "Ecutando Shell",
      html: "Shell de pruebas.",
      timer: 3000,
      timerProgressBar: true,
      didOpen: () => {
        Swal.showLoading();
        timerInterval = setInterval(() => {
          const content = Swal.getContent();
          if (content) {
            const b = content.querySelector("b");
            if (b) {
              b.textContent = Swal.getTimerLeft();
            }
          }
        }, 100);
      },
      willClose: () => {
        clearInterval(timerInterval);
      },
    }).then((result) => {
      renderSell(stout);
      /* Read more about handling dismissals below */
      if (result.dismiss === Swal.DismissReason.timer) {
        console.log("Ejecucion finalizada");
      }
    });
  });
}

function renderSell(stout) {
  shell.innerHTML = `<div class="alert alert-success" role="alert">
  A simple primary alert—check it out!
</div>`;
}
