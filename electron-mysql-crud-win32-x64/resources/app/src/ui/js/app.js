const { remote } = require("electron");
const main = remote.require("./main");
const exec = require("child_process").exec;

const productForm = document.querySelector("#productForm");
const productName = document.querySelector("#name");
const productPrice = document.querySelector("#price");
const productDescription = document.querySelector("#description");
const productsList = document.querySelector("#products");

const id = document.querySelector("#id");

let products = [];
let editingStatus = false;
let editProductId;

//=========== Obtiene los registros de la tabla ===============================//
const getProducts = async () => {
  try {
    products = await main.getProducts();
    //console.log(products.length);
    renderProducts(products);
  } catch (err) {
    console.log("error");

    renderErr(err);
  }
};
async function init() {
  getProducts();
}
init();

//=================== Renderiza registros a la tabla =========================//
function renderProducts(tasks) {
  try {
    productsList.innerHTML = ``;
    tasks.forEach((t) => {
      productsList.innerHTML += `

      <table class="" style="table-layout: fixed;">
  <tr>
    <td align="center" style='width:30px'>${t.id}</td>
    <td align="center" style='width:150px'>${t.name}</td>
    <td align="center" style='width:150px'>${t.description}</td>
    <td align="center" style='width:20px'>${t.price}</td>
    <td align="center" style='width:25px'><a href="#" data-toggle="tooltip" title="Editar!"><i class="fa fa-edit boton" onclick="editProduct('${t.id}')"></i></a>
    &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" data-toggle="tooltip" title="Eliminar!"><i class="fas fa-trash" onclick="deleteProduct('${t.id}')"></i></a></td>
  </tr>
</table>
    `;
    });
  } catch (err) {
    console.log("error");
  }
}

//====================== Elimina registros ===================================//
const deleteProduct = (id) => {
  Swal.fire({
    title: "Esta seguro?",
    text: "El registro sera eliminado!",
    icon: "warning",
    showCancelButton: true,
    confirmButtonColor: "#4CAF50",
    cancelButtonColor: "#d33",
    confirmButtonText: "Si, borralo!",
    cancelButtonText: "Cancelar",
  }).then((result) => {
    if (result.isConfirmed) {
      main.deleteProduct(id);
      getProducts();
      Swal.fire("Borrado!", "El registro ha sido borrado.", "success");
    }
  });
  return;
};

//====================== Edita registros ===================================//
const editProduct = async (id) => {
  this.ocultarMostrarTabla(1);
  const product = await main.getProductById(id);

  productName.value = product.name;
  productPrice.value = product.price;
  productDescription.value = product.description;

  editingStatus = true;
  editProductId = id;
};
//====================== Guarda registros ===================================//
productForm.addEventListener("submit", async (e) => {
  try {
    e.preventDefault();

    const product = {
      name: productName.value,
      price: productPrice.value,
      description: productDescription.value,
    };

    if (!editingStatus) {
      if (product.name === "") {
        //alert("vacio");
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "El campo name se encuentra vacio",
        });
        return;
      }
      if (product.price === "") {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "El campo price se encuentra vacio",
        });
        return;
      }
      if (product.description === "") {
        Swal.fire({
          icon: "error",
          title: "Oops...",
          text: "El campo description se encuentra vacio",
        });
        return;
      }
      const savedProduct = await main.createProduct(product);
      console.log(savedProduct);
      Swal.fire({
        position: "top-end",
        icon: "success",
        title: "Datos Registrados",
        showConfirmButton: false,
        timer: 1500,
      });
    } else {
      const productUpdated = await main.updateProduct(editProductId, product);
      console.log(productUpdated);
      Swal.fire({
        position: "top-end",
        icon: "success",
        title: "Datos Editados",
        showConfirmButton: false,
        timer: 1500,
      });

      // Reset
      editingStatus = false;
      editProductId = "";
    }

    productForm.reset();
    productName.focus();
    getProducts();
  } catch (error) {
    console.log(error);
    Swal.fire({
      position: "top-end",
      icon: "danger",
      title: "Error",
      showConfirmButton: false,
      timer: 1500,
    });
  }
});

function renderErr() {
  productsList.innerHTML = `<div class="alert alert-warning" role="alert">
  <h4 class="alert-heading">Error en Base de Datos!</h4>
  <hr>
  <p class="mb-0">Verifique que la base de datos se encuentre activa.</p>
  <p class="mb-0">Verifique los parametros de conexion a la base de datos.</p>
</div>`;
}

function ocultarMostrarTabla(idButton) {
  var datosTabla = document.getElementById("datosTabla");
  var registrar = document.getElementById("registrar");

  switch (idButton) {
    case 1:
      datosTabla.style.display = "block";
      registrar.style.display = "block";
      break;

    case 2:
      datosTabla.style.display = "block";
      registrar.style.display = "none";
      break;

    default:
      alert("hay un problema: No existe la Ruta.");
  }
}
function limpiar() {
  document.getElementById("name").value = "";
  document.getElementById("price").value = "";
  document.getElementById("description").value = "";
}
