$("#buscar").on("keyup", function() {
  var value = $(this)
    .val()
    .toLowerCase();
  $("#products tr").filter(function() {
    $(this).toggle(
      $(this)
        .text()
        .toLowerCase()
        .indexOf(value) > -1
    );
  });
});
