#!/bin/bash
# Bucle básico While
contador=1
while [ $contador -le 15 ]
do
     echo "Este es el mensaje número" $contador
     ((contador++))
done
echo "Bucle finalizado"