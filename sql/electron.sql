-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1:3306
-- Tiempo de generación: 03-01-2021 a las 14:28:33
-- Versión del servidor: 10.4.11-MariaDB
-- Versión de PHP: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `electron`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `price` decimal(7,3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `product`
--

INSERT INTO `product` (`id`, `name`, `description`, `price`) VALUES
(343, 'producto 1', 'descripcion 111 1111111111111111111115555555555555555555555 ghghghghjg', '100.000'),
(349, 'producto 2', 'desc 2', '200.000'),
(351, 'producto 3', 'desc 3', '123.000'),
(352, 'producto 4', 'desc 4', '155.000'),
(353, 'producto 5', 'desc 5', '1600.000'),
(354, 'producto 6', 'desc 666', '1400.000'),
(355, 'productom 7', 'desc', '700.000'),
(356, 'producto 8', 'desc 8', '800.000'),
(357, 'producto 9', 'desc', '900.000'),
(358, 'producto 10', 'des', '5670.000'),
(359, 'producto 11', 'desc 11', '1111.000'),
(360, 'producto 12', 'desc 12', '1200.000'),
(361, 'producto 13', 'desc 13', '13.000'),
(363, 'producto 14', 'desc 15', '145.000'),
(364, 'rererngghhggj', 'dsdsd', '3232.000'),
(365, 'producto 16', 'desc 16xdddfsdfsfdsdsdfsdfsdf fdfdsfsd', '122.000'),
(366, 'nuevo produ', 'nuevox\n', '180.000');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=367;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
