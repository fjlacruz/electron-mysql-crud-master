const { BrowserWindow, Notification, Menu, app, shell } = require("electron");
const { getConnection } = require("./database");

let window;

const createProduct = async (product) => {
  try {
    const conn = await getConnection();
    product.price = parseFloat(product.price);
    const result = await conn.query("INSERT INTO product SET ?", product);
    product.id = result.insertId;

    // Notify the User
    // new Notification({
    //   title: "Mantenedor",
    //   body: "Se ha registrado informacion",
    // }).show();

    // Return the created Product
    return product;
  } catch (error) {
    console.log(error);
  }
};

const getProducts = async () => {
  const conn = await getConnection();
  const results = await conn.query("SELECT * FROM product ORDER BY id DESC");
  //console.log(results);
  return results;
};

const deleteProduct = async (id) => {
  const conn = await getConnection();
  const result = await conn.query("DELETE FROM product WHERE id = ?", id);
  return result;
};

const getProductById = async (id) => {
  const conn = await getConnection();
  const result = await conn.query("SELECT * FROM product WHERE id = ?", id);
  return result[0];
};

const updateProduct = async (id, product) => {
  const conn = await getConnection();
  const result = await conn.query("UPDATE product SET ? WHERE Id = ?", [
    product,
    id,
  ]);
  console.log(result);
};
//===== lanzamiento de la aplicacion =========//
function createWindow() {
  window = new BrowserWindow({
    width: 1100,
    height: 800,
    webPreferences: {
      nodeIntegration: true,
    },
  });

  window.loadFile("src/ui/views/crud.html");
  //===== Menu de la aplicacion =========//
  var menu = Menu.buildFromTemplate([
    {
      label: "Menu",
      submenu: [
        {
          label: "Inicio",
          click() {
            window.loadFile("src/ui/views/index.html");
          },
        },
        {
          label: "CRUD",
          click() {
            window.loadFile("src/ui/views/crud.html");
          },
        },
        { type: "separator" },
        {
          label: "Salir",
          click() {
            app.quit();
          },
        },
      ],
    },
    {
      label: "Info",
      click() {
        window.loadFile("src/ui/views/test.html");
      },
    },
    {
      label: "Shell",
      click() {
        window.loadFile("src/ui/views/shell.html");
      },
    },
    {
      label: "View",
      submenu: [
        { role: "reload" },
        { role: "toggleDevTools" },
        { type: "separator" },
        { role: "resetZoom" },
        { role: "zoomIn" },
        { role: "zoomOut" },
        { type: "separator" },
        { role: "togglefullscreen" },
        { type: "separator" },
        { role: "minimize" },
      ],
    },
  ]);
  Menu.setApplicationMenu(menu);
}

module.exports = {
  createWindow,
  createProduct,
  getProducts,
  deleteProduct,
  getProductById,
  updateProduct,
};
