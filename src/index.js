const { createWindow } = require("./main");
const { app } = require("electron");

require('./database');

require('electron-reload')(__dirname);

app.allowRendererProcessReuse = true;
app.whenReady().then(createWindow)



//para crear ejecutable
//npm install electron-packager --save-dev
//npm install electron-packager -g
//electron-packager . --platform=win32 --arch=x64